package com.renseki.app.projectpertamaku

class SecondViewModel {

    /**
     * Akan mengubah input menjadi:
     * <NRP>;<NAMA>;<male|female>
     */
    fun generateData(
            nrp: String,
            name: String,
            isMale: Boolean
    ): String {
        val cleanNrp = nrp.trim()
        val cleanName = name.trim()
        val gender = if (isMale) "male" else "female"
        return "$cleanNrp;$cleanName;$gender"
    }
}